public class SalariedEmployee extends StaffMember implements Comparable<StaffMember>{
    double salary,bonus;

    public SalariedEmployee(int id, String name, String address, double salary, double bonus) {
        super(id, name, address);
        this.salary = salary;
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        return "\nID: "+id+
                "\nName : "+name+
                "\nAddress : "+address+
                "\nSalary : "+salary+
                "\nBonus : "+bonus+
                "\nPayment : "+pay()+
                "\n------------------------------------";
    }

    @Override
    public double pay() {
        return salary+bonus;
    }
    @Override
    public int compareTo(StaffMember s) {
        return name.compareTo(s.name);
    }
}
