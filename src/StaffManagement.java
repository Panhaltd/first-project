import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class StaffManagement {
    private String INVALID = "Invalid Input!";
    private String opt;
    private Pattern pattern = Pattern.compile("\\d+");
    private Pattern pattern2 = Pattern.compile("\\D*");
    private Scanner sc = new Scanner(System.in);
    private TreeSet<StaffMember> list = new TreeSet<StaffMember>();
    public static void main(String[] args) {
        StaffManagement sm = new StaffManagement();
        StaffMember member1 = new Volunteer(1,"Monineath","Phnom Penh");
        StaffMember member2 = new SalariedEmployee(2,"Leakhena","Siem Reap",400,200);
        StaffMember member3 = new HourlyEmployee(3,"Yu Ling","China",15,20);
        sm.list.add(member1);
        sm.list.add(member2);
        sm.list.add(member3);
        while (true){
            for (StaffMember li:sm.list){
                System.out.println(li);
            }
            System.out.println("\n------------------------------------");
            System.out.println("1). Add Employee    2). Edit    3). Remove    4). Exit\n");
            System.out.print("=> Choose an option(1-4) : ");
            sm.opt =sm.sc.next();
            Matcher matcher = sm.pattern.matcher(sm.opt);
            boolean optCheck = matcher.matches();
            if (optCheck) {
                switch (sm.opt){
                    case "1":
                        sm.addEmployee();
                        break;
                    case "2":
                        sm.editEmployee();
                        break;
                    case "3":
                        sm.removeEmployee();
                        break;
                    case "4":
                        System.exit(0);
                    default:
                        System.out.println("Wrong Input!!\n=> Choose Option(1-4): ");
                        break;
                }
            }else{
                System.out.println(sm.INVALID);
            }
        }
    }
    void addEmployee(){
        boolean addCheck=true;
        while (addCheck){
            System.out.println("\n------------------------------------");
            System.out.println("1). Volunteer    2). Hourly Employee    3). Salaried Employee    4). Back\n");
            System.out.print("=> Choose an option(1-4) : ");
            opt =sc.next();
            Matcher matcher = pattern.matcher(opt);
            boolean optCheck = matcher.matches();
            if (optCheck) {
                switch (opt){
                    case "1":
                        addVolunteer();
                        addCheck=false;
                        break;
                    case "2":
                        addHourly();
                        addCheck=false;
                        break;
                    case "3":
                        addSalaried();
                        addCheck=false;
                        break;
                    case "4":
                        addCheck=false;
                        break;
                    default:
                        System.out.println("Wrong Input!!");
                        break;
                }
            }else{
                System.out.println(INVALID);
            }
        }
    }
    void addVolunteer(){
        int vID,k;
        String vName,vAddress;
        boolean checkName = true;
        boolean checkID = true;
        System.out.println("\n=========== INSERT INFO ============");
        while (checkID){
            k=0;
            System.out.print("\n=> Enter Staff Member's ID      : ");
            String strID = sc.next();
            Matcher matcher = pattern.matcher(strID);
            boolean isCheck = matcher.matches();
            if (isCheck){
                vID=Integer.parseInt(strID);
                Iterator<StaffMember> itr = list.iterator();
                while (itr.hasNext()) {
                    StaffMember editMember = itr.next();
                    if (editMember.id == vID) {
                        k=1;
                    }
                }
                if (k==0){
                    while (checkName){
                        sc.nextLine();
                        System.out.print("=> Enter Staff Member's Name    : ");
                        String strName= sc.nextLine();
                        Matcher matcher2 = pattern2.matcher(strName);
                        boolean isCheck2 = matcher2.matches();
                        if (isCheck2){
                            vName = strName.substring(0, 1).toUpperCase() + strName.substring(1);
                            System.out.print("=> Enter Staff Member's Address : ");
                            vAddress=sc.nextLine();
                            StaffMember newMember = new Volunteer(vID,vName,vAddress);
                            list.add(newMember);
                            checkName=false;
                            checkID=false;
                        }else{
                            System.out.println("Characters Only!");
                        }
                    }
                }
                if (k==1){
                    System.out.println("This ID is already accessed!");
                }
            }else{
                System.out.println("Digits Only!");
            }
        }
    }
    void addHourly(){
        String hName,hAddress;
        int hHour,hID,k;
        double hRate;
        boolean checkID=true,checkName = true,checkHour=true,checkRate=true;
        System.out.println("\n=========== INSERT INFO ============");
        while (checkID){
            k=0;
            System.out.print("\n=> Enter Staff Member's ID      : ");
            String strID = sc.next();
            Matcher matcher = pattern.matcher(strID);
            boolean isCheck = matcher.matches();
            if (isCheck){
                hID=Integer.parseInt(strID);
                Iterator<StaffMember> itr = list.iterator();
                while (itr.hasNext()) {
                    StaffMember editMember = itr.next();
                    if (editMember.id == hID) {
                        k=1;
                    }
                }
                if (k==0){
                    while (checkName){
                        sc.nextLine();
                        System.out.print("=> Enter Staff Member's Name    : ");
                        String strName= sc.nextLine();
                        Matcher matcher2 = pattern2.matcher(strName);
                        boolean isCheck2 = matcher2.matches();
                        if (isCheck2){
                            hName = strName.substring(0, 1).toUpperCase() + strName.substring(1);
                            System.out.print("=> Enter Staff Member's Address : ");
                            hAddress=sc.nextLine();
                            while (checkHour){
                                System.out.print("=> Enter Staff's Member Hours   : ");
                                String strHour = sc.next();
                                Matcher matcher3 = pattern.matcher(strHour);
                                boolean isCheck3 = matcher3.matches();
                                if (isCheck3){
                                    hHour=Integer.parseInt(strHour);
                                    while (checkRate){
                                        System.out.print("=> Enter Staff's Member Rate    : ");
                                        String strRate = sc.next();
                                        Matcher matcher4 = pattern.matcher(strRate);
                                        boolean isCheck4 = matcher4.matches();
                                        if (isCheck4){
                                            hRate=Double.parseDouble(strRate);
                                            StaffMember newMember = new HourlyEmployee(hID,hName,hAddress,hHour,hRate);
                                            list.add(newMember);
                                            checkRate=false;
                                            checkHour=false;
                                            checkName=false;
                                            checkID=false;
                                        }else{
                                            System.out.println("Digits Only!");
                                        }
                                    }
                                }else{
                                    System.out.println("Digits Only!");
                                }
                            }
                        }else{
                            System.out.println("Characters Only!");
                        }
                    }
                }
                if (k==1){
                    System.out.println("This ID is already accessed!");
                }
            }else{
                System.out.println("Digits Only!");
            }
        }
    }
    void addSalaried(){
        String sName,sAddress;
        double sBonus,sSalary;
        int sID,k;
        boolean checkID=true,checkName = true,checkSalary=true,checkBonus=true;
        System.out.println("\n=========== INSERT INFO ============");
        while (checkID){
            k=0;
            System.out.print("\n=> Enter Staff Member's ID      : ");
            String strID = sc.next();
            Matcher matcher = pattern.matcher(strID);
            boolean isCheck = matcher.matches();
            if (isCheck){
                sID=Integer.parseInt(strID);
                Iterator<StaffMember> itr = list.iterator();
                while (itr.hasNext()) {
                    StaffMember editMember = itr.next();
                    if (editMember.id == sID) {
                        k=1;
                    }
                }
                if (k==0){
                    while (checkName){
                        sc.nextLine();
                        System.out.print("=> Enter Staff Member's Name    : ");
                        String strName= sc.nextLine();
                        Matcher matcher2 = pattern2.matcher(strName);
                        boolean isCheck2 = matcher2.matches();
                        if (isCheck2){
                            sName = strName.substring(0, 1).toUpperCase() + strName.substring(1);
                            System.out.print("=> Enter Staff Member's Address : ");
                            sAddress=sc.nextLine();
                            while (checkSalary){
                                System.out.print("=> Enter Staff Member's Salary   : ");
                                String strSalary = sc.next();
                                Matcher matcher3 = pattern.matcher(strSalary);
                                boolean isCheck3 = matcher3.matches();
                                if (isCheck3){
                                    sSalary=Double.parseDouble(strSalary);
                                    while (checkBonus){
                                        System.out.print("=> Enter Staff Member's Bonus    : ");
                                        String strBonus = sc.next();
                                        Matcher matcher4 = pattern.matcher(strBonus);
                                        boolean isCheck4 = matcher4.matches();
                                        if (isCheck4){
                                            sBonus=Double.parseDouble(strBonus);
                                            StaffMember newMember = new SalariedEmployee(sID,sName,sAddress,sSalary,sBonus);
                                            list.add(newMember);
                                            checkSalary=false;
                                            checkBonus=false;
                                            checkName=false;
                                            checkID=false;
                                        }else{
                                            System.out.println("Digits Only!");
                                        }
                                    }
                                }else{
                                    System.out.println("Digits Only!");
                                }
                            }
                        }else{
                            System.out.println("Characters Only!");
                        }
                    }
                }
                if (k==1){
                    System.out.println("This ID is already accessed!");
                }
            }else{
                System.out.println("Digits Only!");
            }
        }
    }
    void editEmployee(){
        int k=0;
        boolean editCheck=true;
        int eID;
        System.out.println("\n============ EDIT INFO =============");
        while (editCheck){
            k=0;
            System.out.print("=> Enter Employee's ID to Update : ");
            String strID = sc.next();
            Matcher matcher = pattern.matcher(strID);
            boolean isCheck = matcher.matches();
            if (isCheck){
                eID=Integer.parseInt(strID);
                Iterator<StaffMember> itr = list.iterator();
                while (itr.hasNext()){
                    StaffMember editMember = itr.next();
                    if (editMember.id==eID){
                        k=1;
                        System.out.println(editMember);
                        if(editMember instanceof Volunteer){
                            Volunteer editVolunteer = (Volunteer) editMember;
                            editVolunteer(editVolunteer);
                            editCheck=false;
                            break;
                        }else if (editMember instanceof SalariedEmployee){
                            SalariedEmployee editSalary = (SalariedEmployee) editMember;
                            editSalary(editSalary);
                            editCheck=false;
                            break;
                        }else{
                            HourlyEmployee editHourly = (HourlyEmployee) editMember;
                            editHourly(editHourly);
                            editCheck=false;
                            break;
                        }
                    }
                }
                if (k==0){
                    System.out.println("Employee not found!");
                }
            }else{
                System.out.println(INVALID);
            }
        }
    }
    void editVolunteer(Volunteer editMember){
        String name,address;
        boolean checkName=true;
        System.out.println("\n=========== Enter New INFO ===========");
        while (checkName){
            sc.nextLine();
            System.out.print("\n=> Enter Staff Member's Name    : ");
            String strName= sc.nextLine();
            Matcher matcher2 = pattern2.matcher(strName);
            boolean isCheck2 = matcher2.matches();
            if (isCheck2){
                System.out.print("=> Enter Staff Member's Address  : ");
                address=sc.nextLine();
                name = strName.substring(0, 1).toUpperCase() + strName.substring(1);
                StaffMember editVolunteer = new Volunteer(editMember.id,name,address);
                list.add(editVolunteer);
                list.remove(editMember);
                System.out.println("Update Successfully!");
                checkName=false;
            }else{
                System.out.println("Characters Only!");
            }
        }
    }
    void editSalary(SalariedEmployee editMember){
        String name,address;
        double salary,bonus;
        boolean checkName=true,checkSalary=true,checkBonus=true;
        System.out.println("\n=========== Enter New INFO ===========");
        while (checkName){
            sc.nextLine();
            System.out.print("=> Enter Staff Member's Name    : ");
            String strName= sc.nextLine();
            Matcher matcher2 = pattern2.matcher(strName);
            boolean isCheck2 = matcher2.matches();
            if (isCheck2){
                name = strName.substring(0, 1).toUpperCase() + strName.substring(1);
                System.out.print("=> Enter Staff Member's Address  : ");
                address=sc.nextLine();
                while (checkSalary){
                    System.out.print("=> Enter Staff Member's Salary   : ");
                    String strSalary = sc.next();
                    Matcher matcher3 = pattern.matcher(strSalary);
                    boolean isCheck3 = matcher3.matches();
                    if (isCheck3){
                        salary=Double.parseDouble(strSalary);
                        while (checkBonus){
                            System.out.print("=> Enter Staff Member's Bonus    : ");
                            String strBonus = sc.next();
                            Matcher matcher4 = pattern.matcher(strBonus);
                            boolean isCheck4 = matcher4.matches();
                            if (isCheck4){
                                bonus=Double.parseDouble(strBonus);
                                StaffMember editSalary = new SalariedEmployee(editMember.id,name,address,salary,bonus);
                                list.add(editSalary);
                                list.remove(editMember);
                                System.out.println("Update Successfully!");
                                checkName=false;
                                checkSalary=false;
                                checkBonus=false;
                            }else{
                                System.out.println("Digits Only!");
                            }
                        }
                    }else{
                        System.out.println("Digits Only!");
                    }
                }
            }else{
                System.out.println("Characters Only!");
            }
        }
    }
    void editHourly(HourlyEmployee editMember){
        String name,address;
        int hour;
        double rate;
        boolean checkName=true,checkSalary=true,checkBonus=true;
        System.out.println("\n=========== Enter New INFO ===========");
        while (checkName){
            sc.nextLine();
            System.out.print("=> Enter Staff Member's Name    : ");
            String strName= sc.nextLine();
            Matcher matcher2 = pattern2.matcher(strName);
            boolean isCheck2 = matcher2.matches();
            if (isCheck2){
                name = strName.substring(0, 1).toUpperCase() + strName.substring(1);
                System.out.print("=> Enter Staff Member's Address  : ");
                address=sc.nextLine();
                while (checkSalary){
                    System.out.print("=> Enter Staff Member's Hours   : ");
                    String strHour = sc.next();
                    Matcher matcher3 = pattern.matcher(strHour);
                    boolean isCheck3 = matcher3.matches();
                    if (isCheck3){
                        hour=Integer.parseInt(strHour);
                        while (checkBonus){
                            System.out.print("=> Enter Staff Member's Rate    : ");
                            String strRate = sc.next();
                            Matcher matcher4 = pattern.matcher(strRate);
                            boolean isCheck4 = matcher4.matches();
                            if (isCheck4){
                                rate=Double.parseDouble(strRate);
                                StaffMember editHourly = new HourlyEmployee(editMember.id,name,address,hour,rate);
                                list.add(editHourly);
                                list.remove(editMember);
                                System.out.println("Update Successfully!");
                                checkName=false;
                                checkSalary=false;
                                checkBonus=false;
                            }else{
                                System.out.println("Digits Only!");
                            }
                        }
                    }else{
                        System.out.println("Digits Only!");
                    }
                }
            }else{
                System.out.println("Characters Only!");
            }
        }
    }
    void removeEmployee(){
        boolean removeCheck=true,found=false;
        int rID;
        System.out.println("\n========= REMOVE EMPLOYEE ==========");
        while (removeCheck){
            System.out.print("=> Enter Employee's ID to Remove : ");
            String strID = sc.next();
            Matcher matcher = pattern.matcher(strID);
            boolean isCheck = matcher.matches();
            if (isCheck){
                rID=Integer.parseInt(strID);
                Iterator<StaffMember> itr = list.iterator();
                while (itr.hasNext()){
                    StaffMember removeMember = itr.next();
                    if (removeMember.id==rID){
                        System.out.println(removeMember);
                        list.remove(removeMember);
                        System.out.println("Remove Employee Successfully!");
                        found=true;
                        removeCheck=false;
                        break;
                    }
                }
                if (found==false){
                    System.out.println("Employee not found!");
                }
            }else{
                System.out.println(INVALID);
            }
        }
    }
}
